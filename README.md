# Stress testing KAS

Series of Rake tasks to support stress testing KAS. Run `rake -T` to see the possibilities or inspect the `Rakefile`.

## Initial Setup

1. install dependencies with `bundle`.
1. configure your environment variables to connect to your GitLab instance through GraphQL.
1. dump the GraphQL schema to avoid reloading every time. 
   
Set-up script template:

```shell
bundle

export GITLAB_URL=http://gdk.test:3000 # replace by your gitlab instance
export GITLAB_TOKEN="6LRdRh6ry7Zxy_FLDTsB" # replace by your private token
export KAS_ADDRESS="grpc://172.16.123.1:8150" # replace by your KAS address

rake graphql:dump_schema
```

## Sample usage

```shell
# Start 1 agent
rake stress:execute

# Removes projects from GitLab DB and Namespaces from K8s
rake stress:cleanup

# Start 3 agents
rake stress:execute\[3\]
```

## Troubleshooting

- Sequential `rake stress:execute` runs will fail since project names will collide.
  So, run `rake stress: cleanup` after re-running `rake stress:execute`.