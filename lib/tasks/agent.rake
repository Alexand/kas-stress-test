# frozen_string_literal: true

require_relative '../stress/provisioner'

namespace :stress do
  desc 'Creates agents and projects given a token and KAS address'
  task :execute, [:quantity] do |_t, args|
    args.with_defaults(quantity: 1)
    quantity = args[:quantity].to_i

    puts '...provisioning resources'
    Stress::Provisioner.new.run!(quantity)
  end

  desc 'Cleanup everything'
  task :cleanup do
    p = Stress::Provisioner.new

    puts '...removing projects'
    p.cleanup

    puts '...removing k8s namespaces'
    p.delete_k8s_namespaces
  end
end
