# frozen_string_literal: true

require_relative '../graphql'

namespace :graphql do
  desc 'Dumps graphql schema'
  task :dump_schema do
    puts "...dumping GraphQL schema for #{ENV['GITLAB_URL']}"

    GraphQL::Client.dump_schema(Graphql::HTTP, Graphql::SCHEMA_PATH)
  end
end
