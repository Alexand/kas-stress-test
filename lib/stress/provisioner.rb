# frozen_string_literal: true

require_relative '../graphql'
require 'parallel'
require 'open3'
require 'erb'
require 'base64'

module Stress
  # Provisions projects for the stress test
  class Provisioner

    KAS_ADDRESS = ENV['KAS_ADDRESS']

    # @param [Integer] quantity
    # @return [nil]
    def run!(quantity)
      raise "Can't create less than 1 project" if quantity < 1

      Parallel.each(1..quantity, in_processes: 2) do |index|
        project = Gitlab.create_project(project_and_agent_name(index))
        commit_project_files(project, index)
        res = create_agent(project, index)
        res = create_agent_token(res.data.create_cluster_agent.cluster_agent.id)
        deploy_agent(res.data.cluster_agent_token_create.secret, index)
      end

      puts 'GitLab side resources and K8s secrets created'
      puts 'Creating agents...'
    end

    def cleanup
      projects = Gitlab.project_search('stress-test')

      puts "...deleting #{projects.size} projects"

      projects.each_with_index do |project, index|
        Gitlab.delete_project(project.id)

        token64 = "empty"
        agent_namespace = "namespace-#{index}"
        agent_manifest_template = read_agent_fixture

        # create_ns_cmd = "kubectl create ns #{agent_namespace} || true"
        # Open3.capture2(create_ns_cmd)

        agent_manifest_yaml = ERB.new(agent_manifest_template).result(binding)
        delete_manifest(agent_manifest_yaml)
      end
    end

    def delete_k8s_namespaces
      cmd = "kubectl get ns --no-headers=true  | awk '/namespace\-([0-9]|[1-9][0-9]|[1-9][0-9][0-9])/{print $1}' | xargs kubectl delete ns"

      Open3.capture2(cmd)
    end

    private

    def commit_project_files(project, index)
      Gitlab.create_commit(project.id, 'master', 'Creates agent files', project_files(project, index))
    end

    def project_files(project, index)
      [
        { action: 'create',
          file_path: "/.gitlab/agents/#{project_and_agent_name(index)}/config.yaml",
          content: agent_config(project) },
        { action: 'create',
          file_path: '/manifest.yaml',
          content: resource_manifest(index) }
      ]
    end

    def project_full_path(project)
      project.namespace.full_path + '/' + project.path
    end

    def project_and_agent_name(index)
      "stress-test-#{index}"
    end

    def agent_config(project)
      <<~YAML
        gitops:
          manifest_projects:
            - id: #{project_full_path(project)}
      YAML
    end

    def resource_manifest(index)
      <<~YAML
        apiVersion: v1
        kind: ConfigMap
        metadata:
          name: "#{project_and_agent_name(index)}"
          namespace: "namespace-#{index}"
        data:
          counter: "1"
      YAML
    end

    def create_agent(project, index)
      Graphql::Client.query(
        Graphql::CreateAgentQuery,
        variables: { projectPath: project_full_path(project), agentName: project_and_agent_name(index) })
    end

    def create_agent_token(agent_id)
      Graphql::Client.query(Graphql::CreateAgentTokenQuery, variables: {agentId: agent_id, name: 'token-name'})
    end

    def deploy_agent(token, index)
      token64 = Base64.strict_encode64(token.strip)
      agent_namespace = "namespace-#{index}"
      agent_manifest_template = read_agent_fixture

      create_ns_cmd = "kubectl create ns #{agent_namespace} || true"
      Open3.capture2(create_ns_cmd)

      agent_manifest_yaml = ERB.new(agent_manifest_template).result(binding)
      apply_manifest(agent_manifest_yaml)
    end

    def read_agent_fixture
      file_path = Pathname
                  .new(__dir__)
                  .join('../templates/agent-manifest.yaml.erb')

      File.read(file_path)
    end

    def delete_manifest(manifest)
      Open3.popen3('kubectl', 'delete', '--ignore-not-found', '-f', '-') do |stdin, _stdout, stderr, wait_thr|
        stdin.write(manifest)
        stdin.close
        status = wait_thr.value
        warn "Failed to apply manifest:\n#{stderr.read} -- manifest:\n#{manifest}" unless status.success?
      end
    end

    def apply_manifest(manifest)
      Open3.popen3('kubectl', 'apply', '-f', '-') do |stdin, _stdout, stderr, wait_thr|
        stdin.write(manifest)
        stdin.close
        status = wait_thr.value
        warn "Failed to apply manifest:\n#{stderr.read} -- manifest:\n#{manifest}" unless status.success?
      end
    end
  end
end
