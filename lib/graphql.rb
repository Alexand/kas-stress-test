# frozen_string_literal: true

require 'graphql/client'
require 'graphql/client/http'

# Graphql Schema and Client loader
module Graphql
  SCHEMA_PATH = 'lib/graphql-schema.json'

  HTTP = GraphQL::Client::HTTP.new("#{ENV['GITLAB_URL']}/api/graphql") do
    def headers(_context)
      {
        "Authorization": "Bearer #{ENV['GITLAB_TOKEN']}",
        "Content-Type": 'application/json'
      }
    end
  end

  if File.exist?(SCHEMA_PATH)
    Schema = GraphQL::Client.load_schema(SCHEMA_PATH)
    Client = GraphQL::Client.new(schema: Schema, execute: HTTP)

    CreateAgentQuery = Graphql::Client.parse <<-'GRAPHQL'
      mutation ($projectPath: ID!, $agentName: String!) { 
        createClusterAgent(input: { projectPath: $projectPath, name: $agentName }) {
          clusterAgent {
            id
            name
          }
          errors
        }
      }
    GRAPHQL

    CreateAgentTokenQuery = Graphql::Client.parse <<-'GRAPHQL'
      mutation ($agentId: ClustersAgentID!, $name: String!){
        clusterAgentTokenCreate(input: { clusterAgentId: $agentId, name: $name }) {
          secret
          token {
            createdAt
            id
          }
          errors
        }
      }
    GRAPHQL
  end
end
